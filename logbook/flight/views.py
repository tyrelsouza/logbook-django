from rest_framework import viewsets
from flight.serializers import (
    PlaneSerializer,
    AirportSerializer,
    FlightSerializer,
    UserSerializer,
    GroupSerializer,
)

from flight.models import Flight, Plane, Airport
from django.contrib.auth.models import User, Group


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by("-date_joined")
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class PlaneViewSet(viewsets.ModelViewSet):
    queryset = Plane.objects.all().order_by("tail_number")
    serializer_class = PlaneSerializer


class AirportViewSet(viewsets.ModelViewSet):
    queryset = Airport.objects.all().order_by("icao")
    serializer_class = AirportSerializer


class FlightViewSet(viewsets.ModelViewSet):
    queryset = Flight.objects.all().order_by("-flight_date")
    serializer_class = FlightSerializer
