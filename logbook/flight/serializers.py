from django.contrib.auth.models import User, Group
from rest_framework import serializers
from flight.models import Flight, Plane, Airport

from django.contrib.auth.models import User, Group


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ["url", "username", "email", "groups"]


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ["url", "name"]


class PlaneSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Plane
        fields = ["tail_number", "name", "manufacturer", "model", "engine_count"]


class AirportSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Airport
        fields = ["icao", "link"]


class FlightSerializer(serializers.HyperlinkedModelSerializer):
    plane = serializers.StringRelatedField()
    airport_depart = serializers.StringRelatedField()
    airport_arrive = serializers.StringRelatedField()

    class Meta:
        model = Flight
        fields = [
            "flight_date",
            "plane",
            "instructor",
            "remarks",
            "airport_depart",
            "airport_arrive",
            "landings",
            "airplane_sel_time",
            "airplane_mel_time",
            "cross_country_time",
            "day_time",
            "night_time",
            "actual_instrument_time",
            "simulated_instrument_time",
            "ground_trainer_time",
            "dual_received_time",
            "pilot_in_command_time",
            "total_time",
            "leg",
            "link",
        ]
