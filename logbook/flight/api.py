from rest_framework import routers
from django.urls import path, include
from flight import views

router = routers.DefaultRouter()
router.register(r"flights", views.FlightViewSet)
router.register(r"planes", views.PlaneViewSet)
router.register(r"airports", views.AirportViewSet)
router.register(r"users", views.UserViewSet)
router.register(r"groups", views.GroupViewSet)
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path("", include(router.urls)),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
]
